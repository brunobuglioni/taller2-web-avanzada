from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Administrador(models.Model):
	user = models.OneToOneField(User)
	rut = models.CharField(max_length=100,primary_key=True)
	nombre = models.CharField(max_length=100)
	apellido = models.CharField(max_length=100)
	def __str__(self):
		return (self.nombre + " " + self.apellido)

class Estudiante(models.Model):
	user = models.OneToOneField(User)
	rut = models.CharField(max_length=20,primary_key=True)
	nombre = models.CharField(max_length=100)
	apellido_paterno = models.CharField(max_length=100)
	apellido_materno = models.CharField(max_length=100)
	direccion = models.CharField(max_length=200)
	def __str__(self):
		return (self.nombre + " " + self.apellido_paterno)
		
class Profesor(models.Model):
	user = models.OneToOneField(User)
	rut = models.CharField(max_length=20,primary_key=True)
	nombre = models.CharField(max_length=100)
	apellido = models.CharField(max_length=100)
	def __str__(self):
		return (self.nombre + " " + self.apellido)		

class Carrera(models.Model):
	#sigla= models.CharField(max_length=20,primary_key=True)
	nombre = models.CharField(max_length=100)
	def __str__(self):
		return self.nombre		
		
class MallaCurricular(models.Model):
    carrera = models.ForeignKey(Carrera, on_delete=models.CASCADE)    
    def __str__(self):
        return (self.carrera.nombre+ "-" + str(self.id))
		
class Asignatura(models.Model):
	nombre=models.CharField( max_length=100)
	descripcion=models.CharField(max_length=1000)
	sigla=models.CharField( max_length=10)
	malla=models.ForeignKey(MallaCurricular, on_delete=models.CASCADE)  
	def __str__(self):
		return (self.nombre + " " + self.sigla)		
		
class InstanciaAsignatura(models.Model):
	SEMESTRE = (
		('1', 'Primero'),
		('2', 'Segundo')
	)
	asign = models.ForeignKey(Asignatura, on_delete=models.CASCADE)
	profesor = models.ForeignKey(Profesor, on_delete=models.CASCADE)
	ano = models.CharField( max_length=100)
	semestre = models.CharField( max_length=1, choices=SEMESTRE)
	sigla = models.CharField( max_length=100)
	def __str__(self):
		return (self.sigla + "-" + self.semestre + "-" + self.ano)	

class MatriculaMalla(models.Model):		
	estudiante = models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	malla = models.ForeignKey(MallaCurricular, on_delete=models.CASCADE)
	def __str__(self):
		return (self.estudiante.nombre + " " + self.estudiante.apellido_paterno)	

class InscripcionAsignatura(models.Model):
	ESTADO = (
		('0', 'Terminado'),
		('1', 'Cursando')
	)
	estudiante=models.ForeignKey(Estudiante, on_delete=models.CASCADE)
	instanciaAsginatura=models.ForeignKey(InstanciaAsignatura, on_delete=models.CASCADE)
	estado=models.CharField(max_length=1, choices=ESTADO)
	def __str__(self):
		return (self.estudiante.nombre + " " + self.instanciaAsginatura.sigla)		