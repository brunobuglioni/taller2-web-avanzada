from django.contrib import admin

# Register your models here.

from .models import Administrador,Estudiante,Profesor,Carrera,MallaCurricular,Asignatura,InstanciaAsignatura,MatriculaMalla,InscripcionAsignatura

admin.site.register(Administrador)
admin.site.register(Estudiante)
admin.site.register(Profesor)
admin.site.register(Carrera)
admin.site.register(MallaCurricular)
admin.site.register(Asignatura)
admin.site.register(InstanciaAsignatura)
admin.site.register(MatriculaMalla)
admin.site.register(InscripcionAsignatura)

