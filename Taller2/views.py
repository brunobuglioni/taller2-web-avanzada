from django.shortcuts import render, redirect
from django.http import Http404, HttpResponseForbidden
from django.views.generic import TemplateView
from django.contrib.auth import authenticate, login, logout


#def login(request):
#   return render(request,'login.html')

class login(TemplateView):
	template_name = 'login.html'

	def get(self, request):
		if request.user.is_authenticated:
			return render(request,'perfil.html')
		return render(request, self.template_name);


	def post(self, request):
		data = request.POST
		user = authenticate(username=data['username'],
		password=data['password'])
		if user is not None:
			login(request, user)
			return redirect('perfil.html')
		else:
			return render(request, 'perfil.html')

def perfil(request):
    return render(request,'perfil.html')

