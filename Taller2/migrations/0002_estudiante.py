# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-09-30 21:25
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Taller2', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Estudiante',
            fields=[
                ('rut', models.CharField(max_length=20, primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=100)),
                ('apellido_paterno', models.CharField(max_length=100)),
                ('apellido_materno', models.CharField(max_length=100)),
                ('direccion', models.CharField(max_length=200)),
                ('correo', models.EmailField(max_length=100, unique=True)),
            ],
        ),
    ]
